### Meterian Scala sample project

This is a sample Scala project that shows the Meterian integration. You can see the produced report [here](https://www.meterian.io/projects/?pid=d7047f1f-9d6b-4d8f-a35d-beb053720c75).
This project uses the [dockerized client](https://docs.meterian.io/the-meterian-client-dockerized) as a base image. It contains all the required tools and provices a working implementation of the scanner in a pipeline.

Documentation about the GitLab integration can be found [here](https://docs.meterian.io/ci-server-integrations/gitlabcicd)

Where this code come from?
This project is based on a GitLab Project Template, we are not responsibile for the vulnerable components it contains.
